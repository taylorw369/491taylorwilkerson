﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using _491TaylorWilkerson.Models;

namespace _491TaylorWilkerson.Controllers.API


{
    public class ItemController : ApiController
    {

        




        private List<Item> items = new List<Item>()
        {
            new Item { name = "item 1" , id = 1},
            new Item { name = "item 2", id = 2 }
        };
        private int idNum = 2;

        [Route("api/item/{ID}")]
        [HttpGet]
        public int GetItem(int ID)
        { 
              //return num multiplied by ten. status code 200


            return ID * 10;
        }

        [HttpGet]
        [ResponseType(typeof(List<Item>))]
        public async Task<IHttpActionResult> GetItemList()
        {
            //return list<> of strings with at least two elements. status code 200
            return Ok(items);
        }


        [Route("api/item/{ID}")]
        [HttpPost]
        [ResponseType(typeof(string))]
        public async Task<IHttpActionResult> CreateItem(string ID)
        {
            //response payload should be input in uppercase. ex. test to TEST. status code 201
            string newUpper = ID.ToUpper();
            Item item = new Item();
            item.name = ID;
            item.id = idNum++;
            this.items.Add(item);

            return Ok(newUpper);
        }


        [Route("api/item/{ID}")]
        [HttpDelete]
        
        public async Task<IHttpActionResult> DeleteItem(int ID)
        {
            //status code 204
            items.Remove(items.ElementAt(ID - 1));
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }


        [Route("api/item/{ID}")]
        [HttpPatch]
        [ResponseType(typeof(IHttpActionResult))]
        public async Task<IHttpActionResult> UpdateItem(int ID, [FromBody]string newName)
        {
           
            Item newItem = items.ElementAt(ID - 1);
            newItem.name = newName;
            //status code 204
            return StatusCode(System.Net.HttpStatusCode.NoContent);
        }





    }
}